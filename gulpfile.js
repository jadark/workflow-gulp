'use strict';
var gulp        = require('gulp'),
    cache       = require('gulp-cache'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload,
    sass        = require('gulp-sass'),
    jade        = require('gulp-jade'),
    concat      = require('gulp-concat'),
    coffee      = require('gulp-coffee'),
    uglify      = require('gulp-uglify'),
    es          = require('event-stream'),
    plumber     = require('gulp-plumber'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
    prefix      = require('gulp-autoprefixer'),
    gutil       = require('gulp-util'),
    sourcemaps  = require('gulp-sourcemaps'),
    compass     = require('compass-importer'),
    fs          = require('fs')
var directorio  = gutil.env.dir || "";

var folderTemplate = './source/jade/'+directorio+'/';
var arrayFilesName = fs.readdirSync(folderTemplate);

gulp.task('clear', function (done) {
  cache.clearAll(done);
});

gulp.task('javascript', ['clear'], function() {
  es.merge(
    gulp.src('source/coffee/'+directorio+'/*.coffee')
        .pipe(plumber())
        .pipe(coffee({bare: true})),
    gulp.src('source/coffee/'+directorio+'/*.js'))
        .pipe(uglify({
          compress:{
            drop_console: false
          }
         }))
        .pipe( gulp.dest('./dist/js/'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('sass', ['clear'], function () {
  gulp.src('./source/sass/'+directorio+'/*.scss')
      .pipe(plumber())
      // .pipe(sourcemaps.init())
      .pipe(sass({
        //CSS Minificado
          importer: compass,
          outputStyle:'compressed'
        }))
      .pipe(prefix({browsers: ['last 2 versions', 'ie 8', 'ie 9', '> 1%', 'Firefox >= 20', 'Opera 12.1','iOS 7'], cascade: false}))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('./dist/css'))
      .pipe(browserSync.reload({stream: true}));
});

gulp.task('templates', ['clear'], function() {
  gulp.src('./source/jade/'+directorio+'/*.jade')
      .pipe(jade({
        pretty: true
      }))
      .pipe(gulp.dest('./dist'))
      .pipe(browserSync.reload({stream: true}));
});

// gulp.task('fonts', function() {
//   gulp.src(['source/vendor/fonts/**/*.{eot,svg,ttf,woff,woff2}'])
//       .pipe(gulp.dest('dist/fonts/'));
// });

gulp.task('image-compress', ['clear'],function () {
  gulp.src('./source/vendor/images/'+directorio+'/*.{png,jpg,jpeg,gif,svg}')
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      optimizationLevel: 3,
      svgoPlugins: [{
        removeViewBox: false
      }],
      use: [pngquant({
        quality: '75-80',
        speed: 4
      })]
    }))
    .pipe(gulp.dest('dist/images/'));
});

gulp.task('pluginsJS', ['clear'], function() {
  gulp.src(['source/vendor/js/*.js'])
      .pipe(gulp.dest('dist/js/vendor/'));
});

gulp.task('pluginsCss', ['clear'], function() {
  gulp.src(['source/vendor/css/*.css'])
      .pipe(gulp.dest('dist/css/vendor/'));
});

gulp.task('serve', ['templates', 'sass', 'javascript'], loadServers() );

function loadServers() {

  var name, portGenerate, a, str, strInit;
  var bs0, bs1, bs2, bs3, bs4, bs5, bs6, bs7, bs8, bs9;

  if (directorio != ""){
    for (var i = 0; i < arrayFilesName.length; i++) {
      name = arrayFilesName[i].replace("jade", "html");
      portGenerate = 3030 + parseInt(i);
      a = 'bs'+i;
      str = a+' = '+'browserSync.create();';
      eval(str);
      strInit = a+'.init({ui: false, open: true, server: "./dist", port: '+portGenerate+', startPath: "'+name+'", index: "'+name+'"});';
      eval(strInit);
    }//end for
  }else{
    var bs = browserSync.create();
    bs.init({
      open: true,
      server: './dist',
      port: 3030
    });
  };//fin else
}//fin loadServers

gulp.task('watch', function () {
  gulp.watch('source/sass/'+directorio+'/*.scss', ['sass']);
  gulp.watch('source/coffee/'+directorio+'/*.js',  ['javascript']);
  gulp.watch('source/jade/'+directorio+'/*.jade',  ['templates']);
  gulp.watch('source/vendor/images/**/*',  ['image-compress']);
  gulp.watch('dist/*.html').on('change', reload);
});

var taskDefault = [
  'templates',
  'sass',
  'javascript',
  'pluginsJS',
  'pluginsCss',
  // 'fonts',
  'watch',
  'image-compress',
  'serve'
];

gulp.task('default', taskDefault);
