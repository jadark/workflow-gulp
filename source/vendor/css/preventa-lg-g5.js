var actionButtons, components, popUp, saveForm, url, valiDoc, validation;

components = function() {
  $("#sliderEquipo").owlCarousel({
    items: 1,
    itemsDesktop: [1000, 1],
    itemsDesktopSmall: [900, 1],
    itemsTablet: [600, 1],
    itemsMobile: [480, 1],
    autoHeight: true,
    navigation: true,
    navigationText: false,
    pagination: false
  });
  return $("#owl-demo").owlCarousel({
    itemsMobile: [480, 1]
  });
};

popUp = function(mensaje, mensaje2) {
  return $.fancybox.open({
    href: '#popGeneral',
    beforeLoad: function() {
      $('.txtPop').html(mensaje);
      return $('.txtPop2').html(mensaje2);
    },
    closeBtn: false
  });
};

url = 'http://apipreventa2.serviciosmovistar.com/';

saveForm = function() {
  return $.post(url + 'api/Registro/GrabarRegistro', $('#formPreventa').serialize(), function(value) {
    if (value.Codigo === '1000') {
      popUp('¡Gracias por registrarte!', 'Enviamos una copia de tu solicitud al correo electrónico que nos brindaste.');
      return $('#formPreventa').trigger('reset');
    } else if (value.Codigo === '1001') {
      popUp('¡Gracias por registrarte!', 'No pudimos enviarte correo de confirmación');
      return $('#formPreventa').trigger('reset');
    } else if (value.Codigo === '1002') {
      return popUp('Error al grabar', 'Vuelve a intentarlo.');
    } else if (value.Codigo === '1003') {
      return popUp('¡Ups! Ya te encuentras registrado', 'Revisa en tu correo la copia de tu primera solicitud para más detalle.');
    }
  });
};

valiDoc = function() {
  return $.post(url + '/api/Registro/ValidarDocumento', $('#formPreventa').serialize(), function(value) {
    if (value.Codigo === '1000') {
      $('input').prop("disabled", false);
      return $('#txtName').focus();
    } else if (value.Codigo === '1003') {
      popUp('¡Ups! Ya te encuentras registrado', 'Revisa en tu correo la copia de tu primera solicitud para más detalle.');
      return $('#txtDni').prop("disabled", true);
    }
  });
};

validation = function() {
  var mail, numero, solotexto;
  $('input[placeholder]').placeholder();
  numero = " '\\@ñÑ+-|*/°!\"#$%&/()=?¡¨*[];:_^`~¬\\,.-{}´+abcdefghijklmnopqrstuvwxyz¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿŒœŠšŸƒ–—‘’‚“”„†‡•…‰€™¿¡";
  solotexto = '\'\\@+-*/°!<>€"#$%&/()=?¡¨*[];:_^`~¬\\,.-{}+|¡¢£¤¥¦§¨©ª«¬®¯°±²³µ¶·¸¹º»¼½¾¿ÀÂÃÄÅÆÇÈÊËÌÎÏÐÒÔÕÖ×ØÙÛÜÝÞßàâãäåæçèêëìîïðòôõö÷ø';
  mail = '\'\\+*/°!ñÑ"#$%&/<>€()=?¡¨*[];:^`~¬\\,{}+|¡¢£¤¥¦§¨©ª«¬®¯°±²³µ¶·¸¹º»¼½¾¿ÀÂÃÄÅÆÇÈÊËÌÎÏÐÒÔÕÖ×ØÙÛÜÝÞßàâãäåæçèêëìîïðòôõö÷ø';
  $('#txtDni,#txtCel').numeric({
    ichars: numero + '<>'
  });
  $('#txtName,#txtAp').alpha({
    ichars: solotexto
  });
  $('#txtMail').alphanumeric({
    ichars: mail
  });
  if (navigator.userAgent.match(/Android/i)) {
    $('#txtDni,#txtCel').keyup(function() {
      return this.value = this.value.replace(/[^0-9]/g, '');
    });
    $('#txtMail').keyup(function() {
      return this.value = this.value.replace(/[^A-Za-z_.0123456789@-]/g, '');
    });
    $('#txtName,#txtAp').keyup(function() {
      return this.value = this.value.replace(/[^A-Za-zñÑáéíóúÁÉÍÓÚ äëïöüÿÄËÏÖÜ]/g, '');
    });
  }
  return $('#formPreventa').validate({
    errorClass: 'error',
    rules: {
      txtNombres: {
        required: true,
        rangelength: [2, 80]
      },
      txtApellidos: {
        required: true,
        rangelength: [2, 80]
      },
      txtNumeroDocumento: {
        required: true,
        min: 1,
        max: 99999998,
        rangelength: [8, 8]
      },
      txtCorreo: {
        required: true,
        email: true
      },
      txtCelular: {
        required: true,
        rangelength: [9, 9]
      },
      chkProteccion: {
        required: true
      }
    },
    messages: {
      txtNombres: {
        required: 'Ingresa un Nombre(s)',
        rangelength: 'Ingresa un Nombre(s)'
      },
      txtApellidos: {
        required: 'Ingresa tus Apellidos',
        rangelength: 'Ingresa tus Apellidos'
      },
      txtNumeroDocumento: {
        min: 'Ingresa un DNI válido',
        max: 'Ingresa un DNI válido',
        required: 'Ingresa DNI',
        rangelength: 'Debe ingresar 8 dígitos'
      },
      txtCorreo: {
        required: 'Ingresa un Email',
        email: 'Ingresa un Email válido'
      },
      txtCelular: {
        required: 'Ingresa un Teléfono de contacto',
        rangelength: 'Debe ingresar 9 dígitos'
      },
      chkProteccion: {
        required: 'Debe aceptar la cláusula de protección de datos'
      }
    },
    showErrors: function(errorMap, errorList) {
      var all, caja, i, j, ref;
      for (i = j = 0, ref = errorList.length; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
        all = errorList[0].message;
        caja = errorList[0].element.id;
      }
      $('.form-errors').html('');
      $('input').removeClass('errorCaja');
      $('.form-errors').html(all);
      return $('#' + caja).addClass('errorCaja');
    }
  });
};

actionButtons = function() {
  $('.btnEnviar').on('click', function() {
    if ($('#formPreventa').valid()) {
      return saveForm();
    }
  });
  $('.btnAceptar').on('click', function() {
    $.fancybox.close(true);
    $('#txtDni').prop("disabled", false);
    $('#txtName, #txtAp,#txtMail, #txtCel,#cboTipoContrato,#AceptaTerminos,#AceptaPolitica').prop("disabled", true);
    $('#txtDni').val('');
    $('#txtNumLineaAporte').hide();
    $('input[placeholder]').placeholder();
    return $('#formPreventa').trigger('reset');
  });
  return $('#txtDni').on('keyup', function() {
    var len;
    len = $(this).val().length;
    if (len === 8) {
      return valiDoc();
    }
  });
};

$(function() {
  components();
  validation();
  return actionButtons();
});
