class detecMobile
  constructor: () ->
    self = @
    self.initialize()
  initialize: ->
    self = @
    self.detection()
  detection:() ->
    self = @
    md = new MobileDetect(window.navigator.userAgent)
    isMobile = /tablet|ipad/i.test(window.navigator.userAgent.toLowerCase())
    if md.phone() isnt null
      $('#caceria-premios').fadeIn(800)
    else
      $('#caceria-premios').remove()
    return

class compartir
  constructor: () ->
    self = @
    self.phoneRegistrado = "<p class=\"number-register\">Este número ya se encuentra registrado</p>"
    #self.urlDev = 'http://apimmcp.dev2.phantasia.pe' #Dev
    self.urlDev = 'http://apimmcp.serviciosmovistar.com' #Prod
    self.$form = $("#form-sorteo")
    self.$sourceApp = []
    self.$transaccion = []
    self.$monto = []
    self.$tipo = []
    self.$celular = []
    self.initialize()
  initialize: ->
    self = @
    self.validate()
    self.events()

  getParameterByName: (name) ->
    url = document.URL
    url = String(url.match(/\?+.+/))
    url = url.replace('?', '')
    url = url.split('&')
    x = 0
    while x < url.length
      p = url[x].split('=')
      if p[0] == name
        return decodeURIComponent(p[1])
      x++
    return

  addMethodCustom: ->
    $.validator.addMethod 'emailCustom', ((value, element) ->
      @optional(element) or /^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i.test(value)
    ), ''
    $.validator.addMethod 'valphone', (value, element) ->
      @optional(element) or /^9+[0-9]{8}/.test(value)
    , ''

  validateFilter: (eleToFilter, filterType) ->
    blacklist = 'Ã‚ÂºÃƒâ€šÃ‚Â¡Ãƒâ€šÃ‚Â°!$%^&*()+=[]\\;Ãƒâ€šÃ‚Â´,/{}|":<>?~`Ã‚Â¢Ã¢Ë†Å¾Ã¢â‚¬Å“Ã¢â‚¬ÂÃ¢â€° Ã‚ÂªÃ‚Â·ÃƒÂ·Ã‚Â»Ã‚Â¿Ã‚Â«\u2764\u2713\u2600\u2605\u2602\u265E\u262F\u262D\u2622\u20AC\u260E\u221E\u2744\u266B\u20BD\u00A3\u00A4\u00A5\u00A6\u00A7\u00A8\u00A9\u00AA\u00AB\u00BB\u00AC\u00AE\u00AF\u00B0\u00B1\u00B5\u00B6\u00B7\u00BA\u00D7\u00C0\u00C1\u00C2\u00C3\u00C4\u00C5\u00C6\u00C7\u00C8\u00C9\u00CA\u00CB\u2620\u2621\u2622\u2623\u2624\u2625\u2626\u2627\u2628\u2629\u262A\u262B\u262C\u262D\u262F\u2630\u2631\u2632\u2633\u2634\u2635\u2636\u2637\u2638\u2639\u263a\u263b\u263c\u263d\u263e\u263f\u2640\u2641\u2642\u2643\u2644\u2645\u2646\u2647\u2648\u2649\u264A\u264B\u264C\u264D\u264F\u2650\u2651\u2652\u2653\u2654\u2655\u2656\u2657\u2658\u2659\u265a\u265b\u265c\u265d\u265e\u265f\u2664\u2661\uFFE5\uFFE6\uFFED\u00D7\u20A9\u2022\u2023\u2024\u2025\u2026\u2027\u2028\u2029\u2032\u2033\u2034\u2035\u2036\u2037\u2038\u2039\u203A\u203B\u203C\u203D\u203E\u203F\u220F\u0060\u007e\u005c\u007c\u003c\u003e\u007b\u007d\u005b\u005d\u2667\u2662\u2661\u2664\u25a0\u25a1\u25cf\u25cb\u2022\u00b0\u2606\u25aa\u00a4\u300a\u300b\u00a5\u20a9\u2665\u2299'
    switch filterType
      when 'numeric'
        $(eleToFilter).numeric
          allowPlus: false
          allowMinus: false
          allowDecSep: false
          allowThouSep: false
          allowNumeric: true
          min: 0
        return
      when 'text'
        eleToFilter.alpha
          allow: '\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1\u00FC\u00DC'
          disallow: blacklist + '@-_.'
        return
      when 'textOnly'
        eleToFilter.alphanum disallow: 'Ãƒâ€šÃ‚Â¡Ãƒâ€šÃ‚Â°Ãƒâ€šÃ‚Â¨'
        return
      when 'email'
        eleToFilter.alphanum
          allow: '@-_.'
          allowSpace: false
          disallow: blacklist
        return

  validate: () ->
    self = @
    self.addMethodCustom()
    nombres = $('input[name="nombres"]')
    dni = $('input[name="dni"]')
    celular = $('input[name="celular"]')
    email = $('input[name="email"]')

    self.validateFilter celular,"numeric"
    self.validateFilter nombres,"text"
    self.validateFilter email,"email"

    #Si el parametro tiene mas de 9 caracteres es CE
    if self.getParameterByName("dni").length >= 9
      #CE es alfanumerico
      self.validateFilter dni, "textOnly"
      #CE tiene como mínimo 9 y como maximo 12 caracteres
      dnimin = 9
      dnimax = 12
      #Mensajes de error para CE
      required_message = "Ingresar un CE"
      length_message = "Ingresar un CE v\u00e1lido"
      #Cambiamos el placeholder y maxlength para un CE
      dni.attr('placeholder', 'Carnet de Extranjeria');
      dni.attr('maxlength', '12');
    #De lo contrario es DNI
    else
      #DNI es solo numérico
      self.validateFilter dni,"numeric"
      #DNI tiene 8 caracteres fijos
      dnimin = 8
      dnimax = 8
      #Mensajes de error para DNI
      required_message = "Ingresar un DNI"
      length_message = "Ingresar un DNI v\u00e1lido"

    self.$form.validate
      "onsubmit": false
      "rules":
        celular:
          required: true
          valphone: true
          maxlength: 9
        nombres:
          required: true
          minlength: 4
          maxlength: 100
        dni:
          required: true
          minlength: dnimin
          maxlength: dnimax
        email:
          required: true
          emailCustom: true
      "messages":
        celular:
          required: "Ingresar Celular"
          valphone: "Ingresar Celular v\u00e1lido"
          maxlength: 'Ingresar Celular v\u00e1lido'
        nombres:
          required: "Ingresar un Nombre"
          minlength: "Ingrese un Nombre v\u00e1lido"
        dni:
          required: required_message
          minlength: length_message
          maxlenght: length_message
        email:
          required: "Ingresa un E-mail"
          emailCustom:"Ingresa un E-mail v\u00e1lido"

      "onfocusout": (element) ->
        this.element(element)
        return

      "errorPlacement":(error, element) ->
        $element =  $(element)
        type = $element.prop("type").toLowerCase()
        if type is "checkbox"
          $element.parents(".box_checkbox").append(error)
        else
          $element.parent(".trform").append(error)

  validateNumber: () ->
    self = @
    url = self.urlDev + '/api/Registro/ValidarRegistro'
    parameter ='celular'+'='+self.$celular
    $.ajax
      type: 'POST'
      url: url
      crossDomain: true
      dataType: "json"
      processData: true
      data:parameter
      success: (resp, textStatus, jqXHR) ->
        if resp.Codigo is "1002"
          $('#dni').val(resp.DNI).prop('disabled',false)
          $('#nombres').val(resp.Nombres).prop('disabled',false)
          $('#email').val(resp.Email).prop('disabled',false)
          $("#btnsend").prop('disabled',false)
        else if resp.Codigo is "1001"
          $('#dni').val("").prop('disabled',false)
          $('#nombres').val("").prop('disabled',false)
          $('#email').val("").prop('disabled',false)
          $("#btnsend").prop('disabled',false)
          return
    return

  setTransCookie: (tid, expires) ->
    tids = @getTransCookie()
    if(tids.length > 0)
      tids = tids.join(',') + ',' + tid
    else
      tids = tid
    cookie = 'tids=' + tids + '; expire=' + expires
    document.cookie = cookie
    return

  getTransCookie: () ->
    name = 'tids='
    ca = document.cookie.split(';')
    i = 0
    while i < ca.length
      c = ca[i]
      while c.charAt(0) is ' '
        c = c.substring(1)
      if c.indexOf(name) is 0
        tids = c.substring(name.length, c.length)
        return tids.split(',')
      i++
    []

  isValidTransaction: (tid) ->
    tids = @getTransCookie()
    return $.inArray(tid,tids) < 0

  validateFromSend:() ->
    self = @

    tid = @getParameterByName('idTransaccion') or false

    if !tid
      $('#trans-error').show()
      $('#trans-error').html('No puedes participar')
      return

    ### Usando Cookies
    if !@isValidTransaction(tid)
      $('#trans-error').show()
      $('#trans-error').html('Ya estás registrado')
      return
    ###

    url = self.urlDev + '/api/Registro/ValidarRegistroTransaccion'
    parameter = 'idtransaccion='+tid
    $.ajax
      type: 'POST'
      url: url
      crossDomain: true
      dataType: "json"
      processData: true
      data: parameter
      success: (resp, textStatus, jqXHR) ->
        #Si la transacción no existe
        if resp.Codigo is "1001"
          if self.$form.valid()
            url = self.urlDev + '/api/Registro/GrabarRegistro'
            parameter = self.$form.serialize()
            dataForm = parameter+'&'+'transaccion'+'='+self.$transaccion+'&'+'monto'+'='+self.$monto+'&'+'tipo'+'='+self.$tipo+'&'+'idTransaccion'+'='+tid
            #console.log dataForm
            $.ajax
              type: 'POST'
              url: url
              crossDomain: true
              dataType: "json"
              processData: true
              data: dataForm
              success: (resp, textStatus, jqXHR) ->
                if resp.Codigo is "1001"
                  #Set cookie
                  #self.setTransCookie(tid)
                  #Show thank you pop up
                  $('.box-title').fadeOut '350'
                  $('.banner-img').fadeOut '350'
                  $('#form-sorteo').fadeOut '350', ->
                    $('.thank-you').fadeIn(350)
                    return
                  self.$form.trigger "reset"
                  return
        #Si la transacción existe
        else if resp.Codigo is "1002"
          $('#trans-error').show()
          $('#trans-error').html('Ya estás registrado')
          return
    return

  events:() ->
    self = @
    self.$sourceApp = @getParameterByName("source") or ''
    self.$transaccion = @getParameterByName("transaccion") or ''
    self.$monto = @getParameterByName("monto") or ''
    self.$tipo = @getParameterByName("tipoLinea") or ''

    # if self.$sourceApp
    #   detectMobile = new detecMobile()
    # else
    #   $('#caceria-premios').remove()
    #   return

    $('#btnsend').on "click",(e) ->
      e.preventDefault()
      self.validateFromSend()
      return

    # if self.$celular and self.$celular.length is 9
    #   $('#nombres').val(nombresData)
    #   $('#dni').val(self.$tipoLogin)
    #   $('#celular').val(self.$celular)
    #   $('#attachpar').val(self.$celular)
    #   $("#EnviarRegistroBase").trigger "click"

    $('#celular').on 'keyup', ->
      $this = $(this).val().length
      $numberRegister = $('.number-register')
      if $this is 9 and $(this).val().substring(0,1) is '9'
        self.$celular = $(this).val()
        self.validateNumber()
      else
        self.$form.validate().resetForm()
        $('#dni').val("").prop('disabled',true)
        $('#nombres').val("").prop('disabled',true)
        $('#email').val("").prop('disabled',true)
        $("#btnsend").prop('disabled',false)
      return

    return

do() ->
  compartir = new compartir()
  detectMobile = new detecMobile()
  return