class init
  constructor: ()->
    self = @
    @$radio= $("input[type=radio]")
    @$checkbox= $("input[type=checkbox]")
    @$select= $("select")
    self.$form = $("#formData")
    self.initialize()

  initialize: ->
    self = @
    self.load()
    self.tab()
    self.accordeon()
    self.validate()
    self.preventa()
    self.fixPlaceholder()
    ##$('.inputtext').placeholder()
    return

  addMethodCustom: ->
    $.validator.addMethod 'emailCustom', ((value, element) ->
      @optional(element) or /^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i.test(value)
    ), ''
    $.validator.addMethod 'ruc', ((value, element) ->
      @optional(element) or /(^1|2)+0[0-9]{9}$/i.test(value)
    ),''

  load: ()->
      self = @
      self.$radio.on "click", self.events
      self.$checkbox.on "change", self.events
      self.$select.on "change", self.events
      self.trigger(self.$radio, self.$checkbox, self.$select);
      return

  events: ()->
    $this = $(this)
    tagName = $this.prop("tagName").toLowerCase()
    switch tagName
        when "input"
            type = $this.attr("type")
            if type is "radio"
                name = $this.attr("name")
                $('input[name='+name+']').parent().removeClass("checked")
                $this.parent().addClass("chekced")
            else if type is "checkbox"
                checkbox = $this.parent()
                checked = $this.prop "checked"
                if checked then checkbox.addClass("checked") else checkbox.removeClass("checked")
        when "select"
            selectedText= $this.find("option:selected").html()
            $this.prev(".pulldown-text").html(selectedText)
    return

  trigger:($radio,$checkbox,$select)->
      $radio.trigger "click"
      $checkbox.trigger "change"
      $select.trigger "change"
      return

  fixPlaceholder: () ->
    if !Modernizr.input.placeholder
      $boxcaract = $('.boxcontcarac')
      $boxcaract.remove()
      $(".inputtext").each((index, element)->
          placeholder = $(element).attr('placeholder')
          $(element).attr "data-placeholder", placeholder
          $(element).parent().append("<label>"+placeholder+"</label>");
          $(element).on "focus", ->
            $(this).parent().find('label').hide()
            return
          $(element).on "blur", ->
            if $(this).val() is ""
              $(this).parent().find('label').show()
              return
          return
        )
      return

  tab: () ->
    if $('.boxllego').length
      YUI().use 'aui-tabview', (Y)->
        new Y.TabView
          srcNode: '.tabcaract'
        .render()
        return

  accordeon:() ->
    if $('.boxllego').length
      YUI().use 'aui-toggler', (Y) ->
        new (Y.TogglerDelegate)(
          animated: true
          closeAllOnExpand: true
          container: '.accordion'
          content: '.toggler-content'
          expanded: false
          header: '.accordion-heading'
          transition:
            duration: 0.2
            easing: 'cubic-bezier(0, 0.1, 0, 1)')
        return

  validateFilter: (eleToFilter, filterType)->
    switch filterType
      when 'numeric'
        eleToFilter.numeric
          allowPlus: false
          allowMinus: false
          allowDecSep: false
          allowThouSep: false
          allowNumeric: true
          min: 0
        return
      when 'text'
        eleToFilter.alpha
          allow: '\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1\u00FC\u00DC-'
          disallow: 'Â¡Â°!$%^&*()+=[]\\;Â´,/{}|":<>?~`_-¢∞“”≠ª·÷'
        return
      when 'textOnly'
        eleToFilter.alphanum
          disallow: 'Â¡Â°Â¨'
        return
      when 'address'
        eleToFilter.alphanum
          disallow: 'Â¡Â°Â¨'
          allow: '-.'
        return
      when 'email'
        eleToFilter.alphanum
          allow: '@-_.'
          allowSpace: false
          disallow: 'Â¡Â°!$%^&*()+=[]\\;Â´,/{}|":<>?~`_-¢∞“”≠ª·÷'
        return

  validate: ()->
    self = @
    self.addMethodCustom()
    ##Tipo documnto y Numero
    tipodocumento = $('select[name="tipodocumento"]')
    numerodocumento = $('input[name="numerodocumento"]')
    ##Fomularios para DNI y CNE
    formAatipocontrato = $('select[name="formAatipocontrato"]')
    formAcaeq = $('input[name="formAcaeq"]')
    formAporta = $('input[name="formAporta"]')
    formAtipoplan = $('select[name="formB1tipoplan"]')
    formAmodelo = $('select[name="formAmodelo"]')
    formAcapacidad = $('select[name="formAcapacidad"]')
    formAcolor = $('select[name="formAcolor"]')
    formAnombres = $('input[name="formAnombres"]')
    formAapellidos = $('input[name="formAapellidos"]')
    formAemail = $('input[name="formAemail"]')
    formAcelular = $('input[name="formAcelular"]')
    formAdistrito = $('select[name="formAdistrito"]')
    formAdireccion = $('input[name="formAdireccion"]')
    ##formAreferencia = $('input[name="formAreferencia"]')
    formAterminos = $('input[name="formAterminos"]')
    formApdatos = $('input[name="formApdatos"]')
    ##Fomularios RUC registrado
    formB1tipocontrato = $('select[name="formB1tipocontrato"]')
    formB1numeroactual = $('input[name="formB1numeroactual"]')
    formB1tipoplan = $('select[name="formB1tipoplan"]')
    formB1modelo = $('select[name="formB1modelo"]')
    formAcapacidad = $('select[name="formAcapacidad"]')
    formB1color = $('select[name="formB1color"]')
    formB1razonsocial = $('input[name="formB1razonsocial"]')
    formB1representantelegal = $('input[name="formB1representantelegal"]')
    formB1direccionfiscal = $('input[name="formB1direccionfiscal"]')
    formB1emailrepresentante = $('input[name="formB1emailrepresentante"]')
    formB1celularlegal = $('input[name="formB1celularlegal"]')
    formB1distrito = $('select[name="formB1distrito"]')
    formB1direccion = $('input[name="formB1direccion"]')
    ##formB1referencia = $('input[name="formB1referencia"]')
    formB1terminos = $('input[name="formB1terminos"]')
    formB1pdatos = $('input[name="formB1pdatos"]')
    ##Fomularios RUC NO registrado
    formB2numerocelulares = $('select[name="formB2numerocelulares"]')
    formB2razonsocial = $('input[name="formB2razonsocial"]')
    formB2representantelegal = $('input[name="formB2representantelegal"]')
    formB2direccionfiscal = $('input[name="formB2direccionfiscal"]')
    formB2emailrepresentante = $('input[name="formB2emailrepresentante"]')
    formB2celularlegal = $('input[name="formB2celularlegal"]')
    formB2distrito = $('select[name="formB2distrito"]')
    formB2direccion = $('input[name="formB2direccion"]')
    ##formB2referencia = $('input[name="formB2referencia"]')
    formB2terminos = $('input[name="formB2terminos"]')
    formB2pdatos = $('input[name="formB2pdatos"]')

    self.validateFilter numerodocumento, 'numeric'
    ##Fomularios para DNI y CNE
    self.validateFilter formAcaeq, 'numeric'
    self.validateFilter formAporta, 'numeric'
    self.validateFilter formAnombres, 'text'
    self.validateFilter formAapellidos, 'text'
    self.validateFilter formAemail, 'email'
    self.validateFilter formAcelular, 'numeric'
    self.validateFilter formAdireccion, 'address'
    ##self.validateFilter formAreferencia, 'address'
    ##Fomularios RUC registrado
    self.validateFilter formB1numeroactual, 'numeric'
    self.validateFilter formB1razonsocial, 'textOnly'
    self.validateFilter formB1representantelegal, 'text'
    self.validateFilter formB1direccionfiscal, 'address'
    self.validateFilter formB1emailrepresentante, 'email'
    self.validateFilter formB1celularlegal, 'numeric'
    self.validateFilter formB1direccion, 'address'
    ##self.validateFilter formB1referencia, 'address'
    ##Fomularios RUC NO registrado
    self.validateFilter formB2razonsocial, 'textOnly'
    self.validateFilter formB2representantelegal, 'text'
    self.validateFilter formB2direccionfiscal, 'address'
    self.validateFilter formB2emailrepresentante, 'email'
    self.validateFilter formB2celularlegal, 'numeric'
    self.validateFilter formB2direccion, 'address'
    ##self.validateFilter formB2referencia, 'address'

    self.$form.validate
      "onsubmit": false
      "errorClass": "wrapper-error"
      "errorElement": "div"
      "rules":
        ##Tipo documnto y Numero
        tipodocumento:
          required: true
        ##Fomularios para DNI y CNE
        formAatipocontrato:
          required: true
        formAcaeq:
          required:
            depends: (element)->
              val = formAatipocontrato.find('option:selected').data('value')
              if val is 2
                true
              else
                false
          minlength: 9
        formAporta:
          required:
            depends: (element)->
              val = formAatipocontrato.find('option:selected').data('value')
              if val is 3
                true
              else
                false
          minlength: 9
        formAtipoplan:
          required: true
        formAmodelo:
          required: true
        formAcapacidad:
          required: true
        formAcolor:
          required: true
        formAnombres:
          required: true
        formAapellidos:
          required: true
        formAemail:
          required: true
          emailCustom: true
        formAcelular:
          required: true
          minlength: 9
        formAdistrito:
          required: true
        formAdireccion:
          required: true
        ###formAreferencia:
          required: true###
        formAterminos:
          required: true
        formApdatos:
          required: true
        ##Fomularios con RUC Registrado
        formB1tipocontrato:
          required: true
        formB1caeq:
          required:
            depends: (element)->
              val = formB1tipocontrato.find('option:selected').data('value')
              if val is 2
                true
              else
                false
          minlength: 9
        formB1porta:
          required:
            depends: (element)->
              val = formB1tipocontrato.find('option:selected').data('value')
              if val is 3
                true
              else
                false
          minlength: 9
        formB1tipoplan:
          required: true
        formB1modelo:
          required: true
        formB1capacidad:
          required: true
        formB1color:
          required: true
        formB1razonsocial:
          required: true
        formB1representantelegal:
          required: true
        formB1direccionfiscal:
          required: true
        formB1emailrepresentante:
          required: true
          emailCustom: true
        formB1celularlegal:
          required: true
          minlength: 9
        formB1distrito:
          required: true
        formB1direccion:
          required: true
        ###formB1referencia:
          required: true###
        formB1terminos:
          required: true
        formB1pdatos:
          required: true
        ##Fomularios con RUC NO Registrado
        formB2numerocelulares:
          required: true
        formB2razonsocial:
          required: true
        formB2representantelegal:
          required: true
        formB2direccionfiscal:
          required: true
        formB2emailrepresentante:
          required: true
          emailCustom: true
        formB2celularlegal:
          required: true
          minlength: 9
        formB2distrito:
          required: true
        formB2direccion:
          required: true
        ###formB2referencia:
          required: true###
        formB2terminos:
          required: true
        formB2pdatos:
          required: true

      "messages":
        tipodocumento:
          required: "Ingresa tu tipo de Documento",
        numerodocumento:
          required: "Ingresa tu número de Documento"
        ##Fomularios para DNI y CNE
        formAatipocontrato:
          required: "Selecciona un tipo de contrato"
        formAcaeq:
          required: "Ingresa tu N° actual"
          minlength: "Debes ingresar 9 caracteres como mínimo"
        formAporta:
          required: "Ingresa tu N° actual a portar"
          minlength: "Debes ingresar 9 caracteres como mínimo"
        formAtipoplan:
          required: "Selecciona un tipo de plan"
        formAmodelo:
          required: "Selecciona un modelo"
        formAcapacidad:
          required: "Selecciona una capacidad"
        formAcolor:
          required: "Selecciona un color"
        formAnombres:
          required: "Ingresa tus nombres"
        formAapellidos:
          required: "Ingresa tus apellidos"
        formAemail:
          required: "Ingresa un email correcto"
          emailCustom: "Ingresa un email correcto"
        formAcelular:
          required: "Ingresa tu N° celular"
          minlength: "Debes ingresar 9 caracteres como mínimo"
        formAdistrito:
          required: "Selecciona un distrito"
        formAdireccion:
          required: "Ingresa una dirección"
        formAreferencia:
          required: "Ingresa una referencia"
        formAterminos:
          required: "Debes aceptar nuestros Términos y condiciones"
        formApdatos:
          required: "Debes autorizar nuestra politica de protección de datos"
        ##Fomularios con RUC Registrado
        formB1tipocontrato:
          required: "Selecciona un tipo de contrato"
        formB1caeq:
          required: "Ingresa tu N° actual"
          minlength: "Debes ingresar 9 caracteres como mínimo"
        formB1porta:
          required: "Ingresa tu N° actual a portar"
          minlength: "Debes ingresar 9 caracteres como mínimo"
        formB1tipoplan:
          required: "Selecciona un tipo de plan"
        formB1modelo:
          required: "Selecciona un modelo"
        formB1capacidad:
          required: "Selecciona una capacidad"
        formB1color:
          required: "Selecciona un color"
        formB1razonsocial:
          required: "Ingresa una razón social"
        formB1representantelegal:
          required: "Ingresa un representante legal"
        formB1direccionfiscal:
          required: "Ingresa tu dirección fiscal"
        formB1emailrepresentante:
          required: "Ingresa el email del representante"
          emailCustom: "Ingresa un email correcto"
        formB1celularlegal:
          required: "Ingresa el N° celular del representante"
          minlength: "Debes ingresar 9 caracteres como mínimo"
        formB1distrito:
          required: "Selecciona un distrito"
        formB1direccion:
          required: "Ingresa una dirección"
        formB1referencia:
          required: "Ingresa una referencia"
        formB1terminos:
          required: "Debes aceptar nuestros Términos y condiciones"
        formB1pdatos:
          required: "Debes autorizar nuestra politica de protección de datos"
        ##Fomularios con RUC NO Registrado
        formB2numerocelulares:
          required: "Selecciona una cantidad de equipos"
        formB2razonsocial:
          required: "Ingrese su razón social"
        formB2representantelegal:
          required: "Ingresa un representante legal"
        formB2direccionfiscal:
          required: "Ingresa tu dirección fiscal"
        formB2emailrepresentante:
          required: "Ingresa el email del representante"
          emailCustom: "Ingresa un email correcto"
        formB2celularlegal:
          required: "Ingresa el N° celular del representante"
          minlength: "Debes ingresar 9 caracteres como mínimo"
        formB2distrito:
          required: "Selecciona un distrito"
        formB2direccion:
          required: "Ingresa una dirección"
        formB2referencia:
          required: "Ingresa una referencia"
        formB2terminos:
          required: "Debes aceptar nuestros Términos y condiciones"
        formB2pdatos:
          required: "Debes autorizar nuestra politica de protección de datos"


      "onfocusout": (element)->
        this.element(element)
        return

      "errorPlacement":(error, element)->
        ## change of position
        $element = $(element).parents('.trform')
        $element.append(error)

      "highlight":(element)->
        ##add class error
        $element = $(element).parents('.trform')
        $element.addClass('formError')

      "unhighlight":(element)->
        ##remove class error
        $element = $(element).parents('.trform')
        $element.removeClass('formError')


  saveData: ()->
    self = @
    if $("#formData").valid()
      parameter = $(this).parents('.formBox').find('input[name],select[name],textarea[name]').serialize()
      parameter = parameter + "&tipoform=" + $("#tipoformulario").val() + "&tipodocumento=" + $("#tipodocumento").val() + "&numerodocumento=" + $("#numerodocumento").val()
      tipoform = $("#tipoformulario").val()
      tipodoc = $("#tipodocumento").find(':selected').data('value').toString()
      switch tipoform
        when 'formA'
          url = "http://preventaiphone6s.serviciosmovistar.com/api/Registro/GrabarRegistroA1"
        when 'formB1'
          url = "http://preventaiphone6s.serviciosmovistar.com/api/Registro/GrabarRegistroB1"
        when 'formB2'
          url = "http://preventaiphone6s.serviciosmovistar.com/api/Registro/GrabarRegistroB2"

      $(document).ajaxStart ->
        $('#formData').addClass('loadingfade')
        $('.boxloading').fadeIn()
        return

      $.ajax
        type: 'POST'
        url: url
        crossDomain: true
        dataType: "json"
        processData: true
        data:parameter
        success: (responseData, textStatus, jqXHR)->
          dataLayer = window.dataLayer or []
          dataLayer.push
            'event': 'atm.preventa.success'
            'transaction_id': responseData.Informacion
          if tipodoc is '1' or tipodoc is '2'
            $('#formData').fadeOut(() ->
              $('.msgdni').fadeIn()
              $('#formData').removeClass('loadingfade')
              $('.boxloading').css('display','none')
              return
              )
          else if tipodoc is '3'
            $('#formData').fadeOut(() ->
              $('.msgruc').fadeIn()
              $('#formData').removeClass('loadingfade')
              $('.boxloading').css('display','none')
              return
              )
          return

        error: (request, status, error)->
          dataLayer = window.dataLayer or []
          dataLayer.push
            'event': 'atm.preventa.success'
            'motivo_error': responseData.Informacion

    else
      $('#btnsend').parent().addClass('senderror')

  preventa: ()->
    self = @
    $valueTipodoc = []
    $valueModelo = []
    $formBox = $('.formBox')
    optionPi = '<option value="Programa Inteligente" data-value="1">Programa Inteligente S/.199.9</option>'

    $('.btn').on 'click', self.saveData

    $('.backdni, .backruc').on 'click', (e)->
      e.preventDefault()
      $tipoformulario = '#' + $('#tipoformulario').val()
      $($tipoformulario).css('display','none')
      $("#formData").trigger "reset"
      $('.boxloading').removeAttr('style')
      $("#tipodocumento").val("").trigger("change")
      $(this).parents('#msg').fadeOut(()->
        $('#formData').fadeIn()
        return
      )
      return

    if $('#carousel').length > 0 and $().flexslider
        $('#carousel').flexslider
           animation: "slide",
           controlNav: false,
           animationLoop: false,
           directionNav: false,
           slideshow: false,
           itemWidth: 130,
           itemMargin: 5,
           asNavFor: '#slider'

        $('#slider').flexslider
           animation: "slide",
           controlNav: false,
           directionNav: false,
           animationLoop: false,
           slideshow: true,
           sync: "#carousel"

    $('.btnmore').on 'click', (e)->
      e.preventDefault()
      $(this).toggleClass('active').parents('.contentpasos').find('.contentitem').slideToggle()
      if $(this).hasClass('active')
        $(this).find('.text').text('Ver menos')
      else
        $(this).find('.text').text('Ver más')
      return

    $('.btncaract').on 'click', ->
      $(this).toggleClass('active')
      $(this).parent().find('.content-caract').slideToggle()

    ###$('.bannerlink, .llegobtn').on 'click', (e)->
      e.preventDefault()
      if $('#formData').css('display') is 'none'
          location.reload();
      else
        $('html, body').animate
          scrollTop: $('#formData').offset().top, 'slow'
      return###

    ###$('.linkform').on 'click', (e)->
      e.preventDefault()
      $('html, body').animate
        scrollTop: $('#formData').offset().top, 'slow'
      return   ###


    $('.tipocontrato').on 'change', ->
      $value = $(this).find(':selected').data('value').toString()
      $elementParent = $(this).parents('.span6')
      $inputCaeq = $elementParent.find('.inputcaeq')
      $inputPorta = $elementParent.find('.inputporta')
      $inputCaeq.parent().find('.wrapper-error').css('display','none')
      $inputCaeq.parent().removeClass('formError')
      $inputPorta.parent().find('.wrapper-error').css('display','none')
      $inputPorta.parent().removeClass('formError')
      $inputCaeq.parent().addClass('inputdisable')
      $inputPorta.parent().addClass('inputdisable')
      $inputCaeq.val("")
      $inputPorta.val("")
      $inputCaeq.prop('disabled', true)
      $inputPorta.prop('disabled', true)
      if $value is '2'
        $inputCaeq.parent().removeClass('inputdisable')
        $inputCaeq.removeClass('disabled')
        $inputCaeq.prop('disabled', false)
        $inputPorta.parent().addClass('inputdisable')
      else if $value is '3'
        $inputPorta.parent().removeClass('inputdisable')
        $inputPorta.removeClass('disabled')
        $inputPorta.prop('disabled', false)
        $inputCaeq.parent().addClass('inputdisable')
        ###$selectElement.prop('disabled', true).trigger 'blur'###
      return

    $('.formtipoplan').on 'change', ->
      $value = $(this).find(':selected').data('value').toString()
      $selectElement = $(this).parents('.trform')
      $selectElement.find('.listplan').css('display', 'none')
      switch $value
        when '1'
          $selectElement.find('#pInteligente').fadeIn()
          return
        when '2'
          $selectElement.find('#vuela80').fadeIn()
          return
        when '3'
          $selectElement.find('#vuela110').fadeIn()
          return
        when '4'
          $selectElement.find('#vuela140').fadeIn()
          return
        when '5'
          $selectElement.find('#vuela200').fadeIn()
          return

    $('.modelo').on 'change', ->
      $valueModelo = $(this).find('option:selected').data('value').toString()
      $parentElement = $(this).parents('.span6')
      $capacidad = $parentElement.find('.capacidad')
      $color = $parentElement.find('.color')
      $capacidad.val("").trigger("change")
      $color.val("").trigger("change")
      $color.children('option:not(:first)').remove();
      iphone6s ='<option value="Space Gray" data-value="1">Space Gray</option>' +
                '<option value="Gold" data-value="2">Gold</option>' +
                '<option value="Rose Gold" data-value="3">Rose Gold</option>' +
                '<option value="Silver" data-value="4">Silver</option>'

      iphone6splus ='<option value="Space Gray" data-value="1">Space Gray</option>' +
                    '<option value="Gold" data-value="2">Gold</option>' +
                    '<option value="Rose Gold" data-value="3">Rose Gold</option>'

      if $valueModelo is '1'
        $color.append(iphone6s)
      else if $valueModelo is '2'
        $color.append(iphone6splus)

      if $valueModelo isnt ''
        $color.prop("disabled", false)
        $color.parents('.pulldown').removeClass('disabled')
      else
        $capacidad.parents('.trform').removeClass('formError')
        $capacidad.parents('.trform').find('.wrapper-error').css('display', 'none')
        $color.parents('.trform').removeClass('formError')
        $color.parents('.trform').find('.wrapper-error').css('display', 'none')
        $capacidad.prop("disabled", true)
        $color.prop("disabled", true)
        $capacidad.parents('.pulldown').addClass('disabled')
        $color.parents('.pulldown').addClass('disabled')
        return

    $('.color').on 'change', ->
      $valueColor = $(this).find('option:selected').data('value').toString()
      $capacidad = $(this).parents('.input50').find('.capacidad')
      $capacidad.children('option:not(:first)').remove();
      $capacidad.val("").trigger("change")
      spaceGray6s = '<option value="16 GB">16 GB</option>' +
                    '<option value="64 GB">64 GB</option>' +
                    '<option value="128 GB">128 GB</option>'
      gold6s = '<option value="64 GB">64 GB</option>'
      roseGold6s = '<option value="16 GB">16 GB</option>'
      silver6s = '<option value="16 GB">16 GB</option>'+
                 '<option value="64 GB">64 GB</option>'
      spaceGray6splus = '<option value="16 GB">16 GB</option>'+
                        '<option value="128 GB">128 GB</option>'
      gold6splus = '<option value="16 GB">16 GB</option>'
      roseGol6splus = '<option value="16 GB">16 GB</option>'
      silver6splus = '<option value="16 GB">16 GB</option>'

      if $valueModelo is '1' and $valueColor is '1'
        $capacidad.append(spaceGray6s)

      else if $valueModelo is '1' and $valueColor is '2'
        $capacidad.append(gold6s)

      else if $valueModelo is '1' and $valueColor is '3'
        $capacidad.append(roseGold6s)

      else if $valueModelo is '1' and $valueColor is '4'
        $capacidad.append(silver6s)

      else if $valueModelo is '2' and $valueColor is '1'
        $capacidad.append(spaceGray6splus)
      else if $valueModelo is '2' and $valueColor is '2'
        $capacidad.append(gold6splus)
      else if $valueModelo is '2' and $valueColor is '3'
        $capacidad.append(roseGol6splus)
      else if $valueModelo is '2' and $valueColor is '4'
        $capacidad.append(silver6splus)

      if $valueColor isnt ''
        $capacidad.prop("disabled", false)
        $capacidad.parent('.pulldown').removeClass('disabled')
      else
        $capacidad.parents('.trform').removeClass('formError')
        $capacidad.parents('.trform').find('.wrapper-error').css('display', 'none')
        $capacidad.prop("disabled", true)
        $capacidad.parent('.pulldown').addClass('disabled')
      return


    $('#numerodocumento').on 'keyup', ->
      $trform= $('.trform')
      $trform.removeClass('formError');
      $value = $(this).val()
      len = $value.length

      if $valueTipodoc is '1' and len is 8 or $valueTipodoc is '2'  and len is 12 and $('#formA').is(':hidden')
        $('#formA').fadeIn()
        $('html, body').animate
          scrollTop: $('#formA').offset().top - 80, 'slow'
        return

      else if $valueTipodoc is '3' and len is 11
        parameter = "tipodocumento=" + $("#tipodocumento").find("option:selected").text() + "&numerodocumento=" + $("#numerodocumento").val()
        url = "http://preventaiphone6s.serviciosmovistar.com/api/Busqueda/BuscarNegocio"
        $('.boxloading').css('display', 'none')
        $('#formData').removeClass('loadingfade')
        caracter = $value.substring(0,2)
        if caracter is '20'
          $.ajax
            type: 'POST'
            url: url
            crossDomain: true
            dataType: "json"
            processData: true
            data:parameter
            success: (resp, textStatus, jqXHR)->
              if resp.Estado is "OK" ##and $('#formB1').is(':hidden')
                $('#tipoformulario').val('formB1')
                $('#formB2').css('display', 'none')
                $('#formB1').fadeIn()
                $('html, body').animate
                  scrollTop: $('#formB1').offset().top - 80, 'slow'
              else## if $('#formB2').is(':hidden')
                $('#formB1').css('display', 'none')
                $('#formB2').fadeIn()
                $('html, body').animate
                  scrollTop: $('#formB2').offset().top - 80, 'slow'
                $('#tipoformulario').val('formB2')
              return

        if caracter is '10' and $('#formB1').is(':hidden')
          $('#formB2').css('display', 'none')
          ##$("#formB1tipoplan option[data-value='1']").remove();
          $('#formB1').fadeIn()
          $('html, body').animate
            scrollTop: $('#formB1').offset().top - 80, 'slow'
          return
        ###else
          if !$("#formB1tipoplan option[data-value='1']").length
                $("#formB1tipoplan option:first").after(optionPi)
          return###

      $('.boxloading').css('display', 'none')
      $('#formData').removeClass('loadingfade')
      return

    $('#tipodocumento').on 'change', ->
      $valueTipodoc = $(this).find('option:selected').data('value').toString()
      $trform= $('.trform')
      $trform.removeClass('formError');
      $("#formData").validate().resetForm();
      $(".formBox").find('select').val("").trigger("change")
      $('.listplan').css('display', 'none')
      $formBox.fadeOut()
      if $valueTipodoc is ''
        $('#numerodocumento').addClass('disabled')
        $('#numerodocumento').prop('disabled', true)
        $( "#numerodocumento" ).rules( "remove" )
      else
        $('#numerodocumento').removeClass('disabled')
        $('#numerodocumento').prop('disabled', false)
        $('#numerodocumento').rules "add",
          required: true

      if $valueTipodoc is '1'
        $maxlength = 8
        valForm = 'formA'
        $('#numerodocumento').rules "add",
          required: true
          minlength: 8
          ruc:false
          messages: {
            minlength: "Ingresa 8 caracteres"
          }

      else if $valueTipodoc is '2'
        $maxlength = 12
        valForm = 'formA'
        $('#numerodocumento').rules "add",
          required: true
          minlength: 12
          ruc:false
          messages: {
            minlength: "Ingresa 12 caracteres"
          }

      else if $valueTipodoc is '3'
        $maxlength = 11
        valForm = 'formB1'
        $('#numerodocumento').rules "add",
          required: true
          minlength: 11
          ruc:true
          messages: {
            minlength: "Ingresa 11 caracteres"
            ruc: "Ingrese un RUC válido"
          }

      $('#numerodocumento').val ""
      $('#numerodocumento').attr('maxlength', $maxlength)
      $('#tipoformulario').val(valForm)

      if $("#formA").is(":visible") or $("#formB1").is(":visible")  or $("#formB2").is(":visible")
        $("#formData").trigger "reset"
        value = $("#tipodocumento").find("option[data-value=#{$valueTipodoc}]").val()
        $("#tipodocumento").val(value)
        return

    return

  $ ->
    init = new init()
    return
